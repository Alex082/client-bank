package com.example.demo.banking.Service;

import com.example.demo.banking.Dao.AccountDao;
import com.example.demo.banking.Dao.CustomerDao;
import com.example.demo.banking.Modal.Account;
import com.example.demo.banking.Modal.Currency;
import com.example.demo.banking.Modal.Customer;
import com.example.demo.banking.Request.CustomerRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {
    private final CustomerDao customerDao;
    private final AccountDao accountDao;

    @Autowired
    public CustomerService(CustomerDao customerDao, AccountDao accountDao) {
        this.customerDao = customerDao;
        this.accountDao = accountDao;
    }

    public Customer createCustomer(String name, String email, Integer age) {
        Customer customer = new Customer(name, email, age);
        return customerDao.save(customer);
    }

    public boolean updateCustomer(Long customerId, CustomerRequest customerRequest) {
        Customer existingCustomer = customerDao.getOne(customerId);
        if (existingCustomer != null) {

            existingCustomer.setName(customerRequest.getName());
            existingCustomer.setEmail(customerRequest.getEmail());
            existingCustomer.setAge(customerRequest.getAge());

            return true;

        }
        return false;
    }

    public boolean deleteCustomer(Long customerId) {
        Customer customer = customerDao.getOne(customerId);
        if (customer != null) {
            customerDao.delete(customer);
            return true;
        }
        return false;
    }

    public List<Customer> getAllCustomers() {
        return customerDao.findAll();
//        return new ArrayList<>();
    }

    public Customer getCustomerById(Long customerId) {

        return customerDao.getOne(customerId);
    }

    public Customer createCustomer(CustomerRequest customerRequest) {
        String name = customerRequest.getName();
        String email = customerRequest.getEmail();
        Integer age = customerRequest.getAge();

        Customer newCustomer = new Customer(name, email, age);
        return customerDao.save(newCustomer);
    }

    public Account createAccountForCustomer(Long customerId, Currency currency) {
        //System.out.println("Creating account for customer with ID: " + customerId);
        Customer customer = customerDao.getOne(customerId);

        if (customer != null) {
            //System.out.println("Customer found: " + customer);
            Account newAccount = new Account(currency, customer);
            //newAccount.setId(accountDao.getNextAccountId());
            accountDao.save(newAccount);

            // Добавить созданный счет к списку счетов пользователя
            customer.getAccounts().add(newAccount);
            //customerDao.save(customer);

            //System.out.println("Account created: " + newAccount);
            return newAccount;
        }

        return null;
    }

    public boolean deleteAccount(Long customerId, String accountNumber) {
        Customer customer = customerDao.getOne(customerId);

        if (customer != null) {
            Optional<Account> accountToDelete = customer.getAccounts().stream()
                    .filter(account -> account.getNumber().equals(accountNumber))
                    .findFirst();

            if (accountToDelete.isPresent()) {
                customer.getAccounts().remove(accountToDelete.get());
                customerDao.save(customer);

                accountDao.delete(accountToDelete.get());

                return true;
            }
        }

        return false;
    }
}
