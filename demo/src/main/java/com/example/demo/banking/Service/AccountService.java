package com.example.demo.banking.Service;

import com.example.demo.banking.Dao.AccountDao;
import com.example.demo.banking.Modal.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountService {
    private final AccountDao accountDao;

    @Autowired
    public AccountService(AccountDao accountDao) {
        this.accountDao = accountDao;

    }

    public boolean depositMoney(String accountNumber, double amount) {
        Account account = accountDao.findByNumber(accountNumber);
        if (account != null) {
            account.setBalance(account.getBalance() + amount);
            accountDao.save(account);
            return true;
        }
        return false;
    }

    public boolean withdrawMoney(String accountNumber, double amount) {
        Account account = accountDao.findByNumber(accountNumber);
        if (account != null && account.getBalance() >= amount) {
            account.setBalance(account.getBalance() - amount);
            accountDao.save(account);
            return true;
        }
        return false;
    }

    public boolean transferMoney(String sourceAccountId, String destinationAccountId, double amount) {
        Account sourceAccount = accountDao.findByNumber(sourceAccountId);
        Account targetAccount = accountDao.findByNumber(destinationAccountId);

        if (sourceAccount != null && targetAccount != null) {
            if (sourceAccount.getBalance() >= amount) {
                sourceAccount.setBalance(sourceAccount.getBalance() - amount);
                targetAccount.setBalance(targetAccount.getBalance() + amount);

                accountDao.save(sourceAccount);
                accountDao.save(targetAccount);

                return true;
            }
        }
        return false;
    }
}
