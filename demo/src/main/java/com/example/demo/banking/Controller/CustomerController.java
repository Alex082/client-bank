package com.example.demo.banking.Controller;

import com.example.demo.banking.Modal.Account;
import com.example.demo.banking.Modal.Currency;
import com.example.demo.banking.Modal.Customer;
import com.example.demo.banking.Request.CustomerRequest;
import com.example.demo.banking.Service.CustomerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/customers") //......
@Validated
public class CustomerController {
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    //информацию о конкретном пользователе и его счета    //.....
    @GetMapping("/{customerId}")
    public ResponseEntity<Customer> getCustomerById(@PathVariable Long customerId) {
        Customer customer = customerService.getCustomerById(customerId);
        if (customer != null) {
            return ResponseEntity.ok(customer);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    //информацию о всех пользователях //......
    @GetMapping
    public ResponseEntity<List<Customer>> getAllCustomers() {
        List<Customer> customers = customerService.getAllCustomers();

        return ResponseEntity.ok(customers);
    }

    //создаём пользователе //......
    @PostMapping
    public ResponseEntity<Customer> createCustomer(@Validated @RequestBody CustomerRequest customerRequest) {
        Customer customer = customerService.createCustomer(customerRequest);
        if (customer != null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(customer);
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    //изменить данные пользователя //......
    @PutMapping("/{customerId}")
    public ResponseEntity<Customer> updateCustomer(@PathVariable Long customerId, @Validated @RequestBody CustomerRequest customerRequest) {
        if (customerService.updateCustomer(customerId, customerRequest)) {
            //обновилось
            return ResponseEntity.ok().build();
        } else {
            //не обновилось
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    //удалить пользователя //......
    @DeleteMapping("/{customerId}")
    public ResponseEntity<Void> deleteCustomer(@PathVariable Long customerId) {
        if (customerService.deleteCustomer(customerId)) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    //создать счёт конкретному пользователю //.....
    @PostMapping("/{customerId}/accounts")
    public ResponseEntity<Map<String, Object>> createAccountForCustomer(
            @PathVariable Long customerId,
            @RequestBody Map<String, String> request) {

        //Currency currency = Currency.valueOf(request.get("currency"));
        Currency currency;
        try {
            currency = Currency.valueOf(request.get("currency"));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.singletonMap("error", "Invalid currency"));
        }

        Account createdAccount = customerService.createAccountForCustomer(customerId, currency);

        if (createdAccount != null) {
            Customer customer = customerService.getCustomerById(customerId);

            if(customer != null) {

                Map<String, Object> response = new HashMap<>();
                response.put("customer", customer);
                //response.put("account", createdAccount);
                response.put("accounts", customer.getAccounts());

                return ResponseEntity.status(HttpStatus.CREATED).body(response);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    //удалить счёт пользователя  //.....
    @DeleteMapping("/{customerId}/accounts/{accountNumber}")
    public ResponseEntity<Void> deleteAccount(
            @PathVariable Long customerId, @PathVariable String accountNumber) {
        if (customerService.deleteAccount(customerId, accountNumber)) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
}
