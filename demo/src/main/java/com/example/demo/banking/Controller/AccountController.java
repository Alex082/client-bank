package com.example.demo.banking.Controller;


import com.example.demo.banking.Dao.AccountDao;
import com.example.demo.banking.Modal.Account;
import com.example.demo.banking.Service.AccountService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/accounts")
@Validated
@CrossOrigin(origins = "http://localhost:3000")
public class AccountController {
    private final AccountService accountService;
    private final AccountDao accountDao;

    public AccountController(AccountService accountService, AccountDao accountDao) {
        this.accountService = accountService;
        this.accountDao = accountDao;
    }

    @GetMapping
    public ResponseEntity<List<Account>> getAllAccount() {
        return ResponseEntity.ok(accountDao.findAll());
    }

    //пополнить счет   работает с одной валютой
    @PostMapping("/deposit/{accountId}")
    public ResponseEntity<Void> depositMoney(@PathVariable String accountId, @RequestParam double amount) {
        if (accountService.depositMoney(accountId, amount)) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    //cнять деньги со счета (принимает номер счета и сумму,
    // выполняется только если на счету достаточно денег)
    @PostMapping("/withdraw/{accountId}")
    public ResponseEntity<Void> withdrawMoney(@PathVariable String accountId, @RequestParam double amount) {
        if (accountService.withdrawMoney(accountId, amount)) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    //перевести деньги на счет (принимает два номера счета и сумму,
    // выполняется только если на счету достаточно денег)
    @PostMapping("/transfer")
    public ResponseEntity<Void> transferMoney(@RequestParam String sourceAccountId, @RequestParam String destinationAccountId, @RequestParam double amount) {
        if (accountService.transferMoney(sourceAccountId, destinationAccountId, amount)) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }
}
