package com.example.demo.banking.Dao;

import com.example.demo.banking.Modal.Account;
import com.example.demo.banking.Modal.Currency;
import com.example.demo.banking.Modal.Customer;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class CustomerDao implements Dao<Customer> {
    private static long customerIdCounter = 1;
    private List<Customer> customerList;

    public CustomerDao() {
        this.customerList = new ArrayList<>();
        //несколько пользователей
        initSampleData();
    }

    private void initSampleData() {
        Customer user1 = new Customer("John", "john@example.com", 30);
        Customer user2 = new Customer("Alice", "alice@example.com", 25);

        //счет конкретного пользователя
        Account account1User1 = new Account(Currency.USD, user1);
        Account account2User1 = new Account(Currency.EUR, user1);
        user1.setId(customerIdCounter++);

        Account account1User2 = new Account(Currency.USD, user2);
        user2.setId(customerIdCounter++);

        //счет для каждого пользователя
        user1.getAccounts().add(account1User1);
        user1.getAccounts().add(account2User1);
        user2.getAccounts().add(account1User2);

        //добавить пользователей в коллекцию
        customerList.add(user1);
        customerList.add(user2);

    }


    @Override
    public Customer save(Customer customer) {
        customer.setId(customerIdCounter++);
        customerList.add(customer);
        return customer;
    }

    @Override
    public boolean delete(Customer customer) {
        return customerList.remove(customer);
    }

    @Override
    public void deleteAll(List<Customer> entities) {
        customerList.removeAll(entities);
    }

    @Override
    public void saveAll(List<Customer> entities) {
        customerList.addAll(entities);
    }

    @Override
    public List<Customer> findAll() {
        return customerList;
    }

    @Override
    public boolean deleteById(long id) {
        return customerList.removeIf(customer -> customer.getId() == id);
    }

    @Override
    public Customer getOne(long id) {
        return customerList.stream()
                .filter(customer -> customer.getId() == id)
                .findFirst()
                .orElse(null);
    }

    public List<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<Customer> customerList) {
        this.customerList = customerList;
    }
}
