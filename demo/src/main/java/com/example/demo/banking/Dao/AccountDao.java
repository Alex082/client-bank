package com.example.demo.banking.Dao;

import com.example.demo.banking.Modal.Account;
import com.example.demo.banking.Modal.Currency;
import com.example.demo.banking.Modal.Customer;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class AccountDao implements Dao<Account> {
    private static long accountIdCounter = 1;
    private List<Account> accountList;
    private String accountNumber;

    public AccountDao() {
        this.accountList = new ArrayList<>();
        //несколько счетов
        initSampleData();
    }

    private void initSampleData() {
        Customer user1 = new Customer("John", "john@example.com", 30);
        Customer user2 = new Customer("Alice", "alice@example.com", 25);

        Account account1 = new Account(Currency.USD, user1);
        account1.setId(accountIdCounter++);

        Account account2 = new Account(Currency.EUR, user1);
        account2.setId(accountIdCounter++);

        //Account account3 = new Account(Currency.UAH, user2);
        //account3.setId(accountIdCounter++);

        //примеры счетов в коллекцию
        accountList.add(account1);
        accountList.add(account2);
        //accountList.add(account3);

        //баланс для каждого счета
        account1.setBalance(1000.0);
        account2.setBalance(500.0);
        //account3.setBalance(200.0);

        //счета конкретного пользователя
        user1.getAccounts().add(account1);
        user1.getAccounts().add(account2);
        //user2.getAccounts().add(account3);
    }

    @Override
    public Account save(Account account) {
        account.setId(accountIdCounter++);
        accountList.add(account);
        return account;
    }

    @Override
    public boolean delete(Account account) {
        return accountList.remove(account);
    }

    @Override
    public void deleteAll(List<Account> entities) {
        accountList.removeAll(entities);
    }

    @Override
    public void saveAll(List<Account> entities) {
        accountList.addAll(entities);
    }

    @Override
    public List<Account> findAll() {
        return accountList;
    }

    @Override
    public boolean deleteById(long id) {
        return accountList.removeIf(account -> account.getId() == id);
    }

    @Override
    public Account getOne(long id) {
        return accountList.stream()
                .filter(account -> account.getId() == id)
                .findFirst()
                .orElse(null);
    }

    public List<Account> getAccountList() {
        return accountList;
    }

    public void setAccountList(List<Account> accountList) {
        this.accountList = accountList;
    }

    public Account findByNumber(String accountNumber) {
        return accountList.stream()
                .filter(account -> account.getNumber().equals(accountNumber))
                .findFirst()
                .orElse(null);
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

}
